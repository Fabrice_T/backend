var express = require('express');
var router = express.Router();

const auth = require('../middleware/auth');

var userController = require('../controller/users')

router.post('/signup', userController.signup);

router.post('/login', userController.login);

router.get('/logout/:id', auth, userController.logout);

// Unsecured, mostly to test fxns
router.get('/', userController.seeAllAcountsInfo);
router.get('/:id', userController.seeAccountInfo);
router.put('/:id', userController.updateAccountInfo);
router.delete('/:id', userController.deleteAccount);


// Secured, require auth token to work, for active use
// router.get('/', auth, userController.seeAllAcountsInfo);
// router.get('/:id', auth, userController.seeAccountInfo);
// router.put('/:id', auth, userController.updateAccountInfo);
// router.delete('/:id', auth, userController.deleteAccount);

module.exports = router;
