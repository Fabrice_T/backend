var express = require('express');
var router = express.Router();
var platController = require('../controller/plat')

/* GET home page. */
// router.get('/', function(req, res, next) {
//   res.render('index', { title: 'Express' });
// });

router.post('/', platController.addPLat);
router.get('/', platController.getAllPlats);
router.get('/:id', platController.getPlat);
router.put('/:id', platController.updatePlat);
router.delete('/:id', platController.deletePlat);


module.exports = router;
