var express = require('express');
var router = express.Router();

const auth = require('../middleware/auth');
var platController = require('../controller/plat')

router.post('/', platController.addMenu);
router.get('/', platController.getAllMenus);
router.get('/:id', platController.getMenu);
router.put('/:id', platController.updateMenu);
router.delete('/:id', platController.deleteMenu);

module.exports = router;