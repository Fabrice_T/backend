const mongoose = require("mongoose");
const Plat = require('./plat');

const MenuSchema = mongoose.Schema({
    name: { type: String, required: true, trim: true },
    description: { type: String, required: true, trim: true },
    plats:{type:[],required: true},
    restaurateurId: {type: String, required: true}
});

module.exports = mongoose.model('Menu',MenuSchema);