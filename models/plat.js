const mongoose = require("mongoose");

const PlatSchema = mongoose.Schema({
    name: { type: String, required: true, trim: true },
    description: { type: String, required: true, trim: true },
    prix: {type: Number, required: true, min:0},
    categorie: { type: String, enum: ['ENTREE', 'RESISTANCE', 'DESSERT'], default: 'RESISTANCE', required: true },
    restaurateurId: {type: String, required: true}
});

module.exports = mongoose.model('Plat',PlatSchema);