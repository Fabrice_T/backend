const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

const User = require('../models/user');

exports.login = (req,res,next) => {
    User.findOne({ email: req.body.email })
       .then(user => {
           if (!user) {
               return res.status(401).json({ message: 'Incorrect login or password'});
           }
           bcrypt.compare(req.body.password, user.password)
               .then(valid => {
                   if (!valid) {
                       return res.status(401).json({ message: 'Incorrect login or password' });
                   }
                   res.status(200).json({
                       userId: user._id,
                       token: jwt.sign(
                        { userId: user._id },
                        'RANDOM_TOKEN_SECRET',
                        // { expiresIn: '24h' }
                        { expiresIn: 30 }
                    )
                   });
               })
               .catch(error => res.status(500).json({ error }));
       })
       .catch(error => res.status(500).json({ error }));
}

exports.signup = (req,res,next) => {
    bcrypt.hash(req.body.password,10)
    .then(hashedPassword=>{
        const user = new User({
            email: req.body.email,
            password: hashedPassword,
            firstName: req.body.firstName,
            lastName: req.body.lastName,
            dateOfBirth: req.body.dateOfBirth,
            role: req.body.role,
        });
        user.save()
        .then(() => res.status(200).json({message: 'User account created successfully!'}))
        .catch((error) => {res.status(500).json({message: "An error occured, User account not created",error})})
    })
    .catch((error) => {res.status(500).json({error})})
}

exports.logout = (req,res,next) => {
    // jwt.
}

exports.seeAccountInfo = (req,res,next) => {
    User.findOne({_id:req.params.id})
    .then((user) => {res.status(200).json(user)})
    .catch((error) => {res.status(500).json({ error:error.message })});
}

exports.updateAccountInfo = (req,res,next) => {
    User.updateOne({_id:req.params.id},{
        ...req.body
    })
    // .then((result) => {res.status(200).json({result});})
    .then((result) =>{this.seeAccountInfo(req,res,next)})
}

exports.deleteAccount = (req,res,next) => {
    User.remove({_id:req.params.id})
    .then(()=>{res.status(200).json({message:'User account was successfully deleted'})})
}

exports.seeAllAcountsInfo = (req,res,next) => {
    User.find()
    .then((users) => {res.status(200).json({users});})
    .catch((error) => {res.status(500).json({ error })})
}
