const Plat = require('../models/plat');
const Menu = require('../models/menu');


exports.addPLat = (req,res,next) => {
    const plat  = new Plat({...req.body})
    plat.save()
    .then(() => res.status(200).json({message: 'New Dish added successfully!'}))
    .catch((error) => {res.status(500).json({ error:error.message })})
}

exports.getPlat = (req,res,next) => {
    Plat.findOne({_id:req.params.id})
    .then((plat) => {res.status(200).json(plat)})
    .catch((error) => {res.status(500).json({ error:error.message })});
}

exports.updatePlat = (req,res,next) => {
    Plat.updateOne({_id:req.params.id},{
        ...req.body
    })
    // .then((result) => {res.status(200).json({result});})
    .then((result) =>{this.getPlat(req,res,next)})
}

exports.deletePlat = (req,res,next) => {
    Plat.remove({_id:req.params.id})
    .then(()=>{res.status(200).json({message:'Dish was successfully deleted'})})
}

exports.getAllPlats = (req,res,next) => {
    Plat.find()
    .then((plats) => {res.status(200).json({plats});})
    .catch((error) => {res.status(500).json({ error })})
}


// For Menus
exports.getAllMenus = (req,res,next) => {
    Menu.find()
    .then((menus) => {res.status(200).json({menus});})
    .catch((error) => {res.status(500).json({ error })})
}

exports.getMenu = (req,res,next) => {
    Menu.findOne({_id:req.params.id})
    .then((menu) => {res.status(200).json(menu)})
    .catch((error) => {res.status(500).json({ error:error.message })});
}

exports.updateMenu = () => {}

exports.deleteMenu = (req,res,next) => {
    Menu.remove({_id:req.params.id})
    .then(() => {res.status(200).json({message:'Menu Successfully Deleted'})})
}

exports.addMenu = (req,res,next) => {
    const menu  = new Menu({...req.body})
    menu.save()
    .then(() => res.status(200).json({message: 'New Menu added successfully!'}))
    .catch((error) => {res.status(500).json({ error:error.message })})
}
